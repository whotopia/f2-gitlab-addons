# f2-gitlab-addons

This project is meant for use with Flectra 2 addon modules.  It contains a bash script which keeps a collection of Flectra community addon repositories updated and further copies the various addons therein into one folder for use by the application.

## Getting started

See comments inside the file update-all.sh 

## Name
f2-gitlab-addons 

## Description
This project is meant for use with Flectra 2 addon modules. It contains a bash script which keeps a collection of Flectra community addon repositories updated and further copies the various addons therein into one folder for use by the application.


## Installation
Create a directory, ideally within a user home folder that has python virtual env for Flectra.  See bash script for several variables that you need to set.

## Usage
You can run this script manually, or e.g. run it as part of a cron job which automatically keeps your Flectra addons up to date. 

## Support
No support beyond sharing this file will be provided. 

## Roadmap
This does what I need so I'm not planning to improve it much.  There is also a version of this file which can instead be used on OCA repositories which automatically also does F2 conversion. However this isn't really safe/practical for 
production use as there are often conflicts or issues.  Also, the Flectra GitLab repositories are becoming really useful and complete. 

## Contributing
If you have ideas or improvements, please comment here on GitLab. 


## Authors and acknowledgment
Show your appreciation to those who have contributed to the project.

## License
Do with this as you may.  I'd appreciate it if you sighted me as thee origin author. 


