#!/bin/bash
# This script downloads all of the Flectra 2 repositories that I happen to be interested in.
# In some cases the Flectra team has not yet fully / officially supported migration of modules to 2.0
# in that situation, where I am okay with playing with code that may be experimental, I use either the
# v2.0.8.1 branch or the 2.0-fixed branch
# YOUR SITUATION MAY BE DIFFERENT
# Flectra is also actively updating repositories.  So, if you do decide to use a non 2.0 branch, it
# is important that you occasionally look to see if there has been a 2.0 transition/update
#

mkdir -p $HOME/src/f2-addons-native2
cd $HOME/src/f2-addons-native2

git clone -b 2.0 https://gitlab.com/flectra-community/account-analytic.git
git clone -b 2.0 https://gitlab.com/flectra-community/account-budgeting.git
git clone -b 2.0 https://gitlab.com/flectra-community/account-closing.git
git clone -b 2.0 https://gitlab.com/flectra-community/account-consolidation.git
git clone -b 2.0 https://gitlab.com/flectra-community/account-financial-reporting.git
git clone -b 2.0 https://gitlab.com/flectra-community/account-financial-tools.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/account-fiscal-rule.git
git clone -b 2.0 https://gitlab.com/flectra-community/account-invoice-reporting.git
git clone -b 2.0 https://gitlab.com/flectra-community/account-invoicing.git
git clone -b 2.0 https://gitlab.com/flectra-community/account-payment.git
 git clone -b v2.0.8.1 https://gitlab.com/flectra-community/account-reconcile.git
git clone -b 2.0 https://gitlab.com/flectra-community/bank-payment.git
git clone -b 2.0 https://gitlab.com/flectra-community/bank-statement-import.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/brand.git
git clone -b 2.0 https://gitlab.com/flectra-community/business-requirement.git
git clone -b 2.0 https://gitlab.com/flectra-community/calendar.git
git clone -b 2.0 https://gitlab.com/flectra-community/commission.git
git clone -b 2.0 https://gitlab.com/flectra-community/community-data-files.git
git clone -b 2.0 https://gitlab.com/flectra-community/connector.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/connector-interfaces.git
git clone -b 2.0 https://gitlab.com/flectra-community/connector-telephony.git
 git clone -b v2.0.8.1 https://gitlab.com/flectra-community/contract.git
git clone -b 2.0 https://gitlab.com/flectra-community/credit-control.git
git clone -b 2.0 https://gitlab.com/flectra-community/crm.git
git clone -b 2.0 https://gitlab.com/flectra-community/currency.git
git clone -b 2.0 https://gitlab.com/flectra-community/data-protection.git
git clone -b 2.0 https://gitlab.com/flectra-community/delivery-carrier.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/dms.git
git clone -b 2.0 https://gitlab.com/flectra-community/donation.git
git clone -b 2.0 https://gitlab.com/flectra-community/e-commerce.git
git clone -b 2.0 https://gitlab.com/flectra-community/edi.git
git clone -b 2.0 https://gitlab.com/flectra-community/event.git
git clone -b 2.0 https://gitlab.com/flectra-community/field-service.git
git clone -b 2.0 https://gitlab.com/flectra-community/geospatial.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/helpdesk.git
git clone -b 2.0 https://gitlab.com/flectra-community/hr.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/hr-attendance.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/hr-expense.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/hr-holidays.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/infrastructure.git
git clone -b 2.0 https://gitlab.com/flectra-community/intrastat-extrastat.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/knowledge.git
git clone -b 2.0 https://gitlab.com/flectra-community/l10n-germany.git
git clone -b 2.0 https://gitlab.com/flectra-community/l10n-switzerland-flectra.git
git clone -b 2.0 https://gitlab.com/flectra-community/maintenance.git
 git clone -b v2.0.8.1 https://gitlab.com/flectra-community/management-system.git
git clone -b 2.0 https://gitlab.com/flectra-community/manufacture.git
git clone -b 2.0 https://gitlab.com/flectra-community/manufacture-reporting.git
git clone -b 2.0 https://gitlab.com/flectra-community/margin-analysis.git
git clone -b 2.0 https://gitlab.com/flectra-community/mis-builder.git
git clone -b 2.0 https://gitlab.com/flectra-community/mis-builder-contrib.git
 git clone -b v2.0.8.1 https://gitlab.com/flectra-community/muk_base.git
 git clone -b v2.0.8.1 https://gitlab.com/flectra-community/muk_dms.git
 git clone -b v2.0.8.1 https://gitlab.com/flectra-community/muk_misc.git
 git clone -b v2.0.8.1 https://gitlab.com/flectra-community/muk_quality.git
 git clone -b v2.0.8.1 https://gitlab.com/flectra-community/muk_web.git
 git clone -b v2.0.8.1 https://gitlab.com/flectra-community/multi-company.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/oca-custom.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/odoo-pim.git
git clone -b 2.0 https://gitlab.com/flectra-community/operating-unit.git
git clone -b 2.0 https://gitlab.com/flectra-community/partner-contact.git
 git clone -b v2.0.8.1 https://gitlab.com/flectra-community/partner-contact-extra.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/payroll.git
git clone -b 2.0 https://gitlab.com/flectra-community/product-attribute.git
git clone -b 2.0 https://gitlab.com/flectra-community/product-configurator.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/product-pack.git
git clone -b 2.0 https://gitlab.com/flectra-community/product-variant.git
git clone -b 2.0 https://gitlab.com/flectra-community/project.git
git clone -b 2.0 https://gitlab.com/flectra-community/project-agile.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/project-reporting.git
git clone -b 2.0 https://gitlab.com/flectra-community/purchase-workflow.git
 git clone -b v2.0.8.1 https://gitlab.com/flectra-community/queue.git
git clone -b 2.0 https://gitlab.com/flectra-community/reporting-engine.git
git clone -b 2.0 https://gitlab.com/flectra-community/report-print-send.git
git clone -b 2.0 https://gitlab.com/flectra-community/rest-framework.git
git clone -b 2.0 https://gitlab.com/flectra-community/rma.git
git clone -b 2.0 https://gitlab.com/flectra-community/sale-reporting.git
git clone -b 2.0 https://gitlab.com/flectra-community/sale-workflow.git
git clone -b 2.0 https://gitlab.com/flectra-community/server-auth.git
git clone -b 2.0 https://gitlab.com/flectra-community/server-backend.git
git clone -b 2.0 https://gitlab.com/flectra-community/server-brand.git
git clone -b 2.0 https://gitlab.com/flectra-community/server-env.git
git clone -b 2.0 https://gitlab.com/flectra-community/server-tools.git
git clone -b 2.0 https://gitlab.com/flectra-community/server-ux.git
git clone -b 2.0 https://gitlab.com/flectra-community/social.git
git clone -b 2.0 https://gitlab.com/flectra-community/stock-logistics-barcode.git
git clone -b 2.0 https://gitlab.com/flectra-community/stock-logistics-warehouse.git
 git clone -b 2.0-fixed https://gitlab.com/flectra-community/storage.git
 git clone -b v2.0.8.2 https://gitlab.com/flectra-community/timesheet.git
git clone -b 2.0 https://gitlab.com/flectra-community/vertical-association.git
git clone -b 2.0 https://gitlab.com/flectra-community/web.git
git clone -b 2.0 https://gitlab.com/flectra-community/website.git
git clone -b 2.0 https://gitlab.com/flectra-community/website-cms.git
git clone -b 2.0 https://gitlab.com/flectra-community/wms.git

