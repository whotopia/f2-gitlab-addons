#!/bin/bash
# ~/bin/update-all.sh 
# This Bash script is used to synchronise Flectra 2 addon folders from gitlab 
#
# Copyright 2022 Steven Uggowitzer <steven@entuura.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#

#  I install Flectra under a unique Linux user per Flectra instance like this:
#    /home/f2-company                   <-  Flectra Directory
#    /home/f2-company/f2-venv           <-  Python3 user virtual environment for this user 
#    /home/f2-company/flectra           <-  The Flectra2 you are running
#    /home/f2-company/f2-local          <-  Directory for session_dir and data_dir 
#    /home/f2-company/logs              <-  Log directory
#    /home/f2-company/conf              <-  Flectra flectra.conf file location
#    /home/f2-company/bin/              <-  Place this script here.  
#    /home/f2-company/tmp/f2-addons-tmp <-  Tempo dir used by this script
#    /home/f2-company/f2-addons         <-  Add this directory to flectra.conf addons_path
##
##
#    This script expects to find an arbitrary set of Flectra 2 addon git repositories 
#    in the directory $DOWNLOAD_DIR 
#    Create the $DOWNLOAD_DIR directory and then at your discretion manually add any of the 
#    major Flectra 2 repositories you would like to have available in your Flectra instance 
#    Remember to directly specify the Branch you need for your particular instance of Flectra.
#    For example:
#          > cd /home/f2-company/f2-addons-native 
#          > git clone -b 2.0 https://gitlab.com/flectra-community/l10n-switzerland-flectra.git
#          > git clone -b 2.0 https://gitlab.com/flectra-community/web.git
#          > copy ~/src/f2-gitlab-addons/update-all.sh .
#          > chmod +x ./update-all.sh 
#      see seed-download-repos.sh in this folder. 

# FLECTRA User.  
# This is needed in case e.g. the script happens to e.g. be run as root.   It sets the 
# permissions correctly for all the associated directories.   
F2_User="f2-company"
# Assumes there is also a group with same name. 
F2_Grp=$F2_User 

DOWNLOAD_DIR=/home/$F2_User/src/f2-addons-native 
BASE_DIR=$(basename $DOWNLOAD_DIR)

# This is a temporary directory used to help with staging addons folders all at the same
# directory level
CVT_DIR=/home/$F2_User/tmp/f2-addons-tmp

# This is the directory which you add to flectra.conf configuration file 
# Remember that you either need to restart Flectra service, or request addons update
# in Flectra to re-read all of the addons folders.  
ADD_DIR=/home/$F2_User/f2-addons

## Some addons require their own additional Python 3 libraries.  Here you have the option
## to install at the Linux OS level, or in our case use the virtual environment to download them
PY_PIP=/home/$F2_User/f2-venv/bin/pip


# Change directory to the where the git repositories are 
cd $DOWNLOAD_DIR

echo "Current Directory: $DOWNLOAD_DIR "
echo "Base Directory: $BASE_DIR "
echo "Flectra Convert Dir: $CVT_DIR"

## Create these directories if they don't exist 
[ -d $CVT_DIR ] || mkdir -p $CVT_DIR 
[ -d $ADD_DIR ] || mkdir -p $ADD_DIR 

# Let the person running the script know what's going on.
echo "Pulling in latest changes for all repositories..."

# Find all git repositories and update them to the latest revision of the particular 
# branch they are on. 
# Also proocess any new Python3 addons which may be required. 
#  - Note that on many Linux Distros it may be easier to install most Python
#    addons at the OS level instead using packages from the distro.  
#    You might find it better to do this manually.  Also, many Python
#    addons may require either other distro-based packages (e.g. -dev ones)
#    or may require full compiler/build environment (build-essential) for
#    installations to work.  The bottom line is that you need to pay attention here. 
for i in $(find . -name ".git" | cut -c 3-); do
    echo "Dealing with: $i"
    # We have to go to the .git parent directory to call the pull command
    cd "$i";
    cd ..;
    # finally pull
    echo "Local branches for $i:"
    git branch
    git config --global pull.rebase true
    git pull
    # If the addon needs more Python stuff automatically install it. 
    if [ -f requirements.txt ]; then
        $PY_PIP install -r requirements.txt 
    fi
    # lets get back to the CUR_DIR
    cd $DOWNLOAD_DIR
done

#  Finally this step copies all of the addon folders with in a repository
#  to the tmp directory which is ultimately rsynced to the final place for Flectra
#  to use. 
cd $DOWNLOAD_DIR
for j in $(find . -name "__manifest__.py" | cut -c 3-); do
    ADDON_DIR=$(dirname $j)
    PROC_DIR="${ADDON_DIR##*/}"
    FADDON_DIR="$CVT_DIR/$PROC_DIR"

    echo "Processing dir: $ADDON_DIR  Flectra dir: $FADDON_DIR"
    echo "Deleting addon dir if exists:  $FADDON_DIR"
    [ -d $FADDON_DIR ] || rm -rf $FADDON_DIR
    echo "Copying  $ADDON_DIR to $CVT_DIR"
    cp -raf $ADDON_DIR $CVT_DIR/
    echo "..... Done."
done

# This command in one swoop syncs any new stuff from the tmp dir to the 
# addons dir used by Flectra.  It also cleans up anything which might need
# to be deleted.  Therefore you can also delete git repos, and the will subsequently
# be deleted from the directory exposed to Flectra. 
rsync -avh $CVT_DIR/ $ADD_DIR  --delete

## This will chown all files to the correct user and group.  
##  The shopt commands are needed to deal with .git folders. 
shopt -s dotglob; chown -R $F2_User:$F2_Grp $CVT_DIR; shopt -u dotglob
shopt -s dotglob; chown -R $F2_User:$F2_Grp $ADD_DIR; shopt -u dotglob
shopt -s dotglob; chown -R $F2_User:$F2_Grp $DOWNLOAD_DIR; shopt -u dotglob


echo "Complete!"
