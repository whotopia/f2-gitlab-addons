#!/bin/bash
# Put this in $HOME/bin/update-daily.sh
# expects to find the $HOME/update-all.sh
#  and $HOME/flectra2 for the flectra code
#
# Run this from a cron

[ ! -d $HOME/logs ] || mkdir -p $HOME/logs

# Update the Flectra tree to latest
cd $HOME/flectra2
git pull  >         $HOME/logs/update-f2.log  2>&1 

# Update the Flectra Modules
cd  $HOME/bin
./update-all.sh > $HOME/logs/update-addons.log 2>&1 


